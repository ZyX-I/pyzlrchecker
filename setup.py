#!/usr/bin/env python
# vim:fileencoding=utf-8:sts=4:sw=4:et

import os
import sys

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
try:
    README = open(os.path.join(here, 'README.rst')).read()
except IOError:
    README = ''

setup(
    name='PyZLRChecker',
    version='alpha',
    description='Set of packages to work with *.lib.ru sites',
    long_description=README,
    classifiers=[],
    author='Nikolay Pavlov',
    author_email='kp-pav@yandex.ru',
    url='https://bitbucket.org/ZyX_I/',
    scripts=[
        'scripts/zlrchecker',
    ],
    packages=find_packages(exclude=('tests', 'tests.*')),
    include_package_data=True,
    zip_safe=False,
    install_requires=['html5lib', 'beautifulsoup4'],
)
