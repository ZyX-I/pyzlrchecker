# vim:fileencoding=utf-8:sts=4:sw=4:et

from itertools import repeat, chain
from bs4 import BeautifulSoup as parse
from urllib2 import urlopen, HTTPError
from xml.etree.ElementTree import SubElement
from collections import namedtuple

fb2_to_html_tags = (
    ('emphasis',      ('i', 'em', 'address', 'dfn', 'q')),
    ('strong',        ('b', 'strong')),
    ('p',             ('p', 'code', 'pre', 'dd', 'dt', 'li') +
                      tuple(('h' + str(i) for i in range(8)))),
    ('strikethrough', ('s', 'del', 'strike')),
    (None,            ('abbr', 'bdo', 'caption', 'center', 'cite', 'small',
                       'font', 'dl', 'ul', 'ol')),
    ('table',         ('table',)),
    ('tr',            ('tr',)),
    ('td',            ('td', 'th')),
    ('image',         ('img',)),
    ('a',             ('a',)),
)

html_to_fb2_tags = dict(
    reduce(chain, (zip(htmltags, repeat(fb2tag))
                   for fb2tag, htmltags in fb2_to_html_tags))
)


ImageData = namedtuple('ImageData', 'name mime image fb2tag')


class HTMLTransformer(object):
    def __init__(self, fb2):
        self.fb2 = fb2
        self.fb2_body = fb2.find('body')
        self.img_idx = 0
        self.images = []
        self.tobody()

    def tobody(self):
        self.fb2_prevtags = [self.fb2_body]
        self.fb2_curtag = self.fb2_body

    def down(self, tagname):
        tag = SubElement(self.fb2_curtag, tagname)
        if len(self.fb2_prevtags) == 1 and tagname != 'p':
            raise ValueError('Second tag must be <p>')
        self.fb2_prevtags.append(tag)
        self.fb2_curtag = tag

    def up(self):
        self.fb2_prevtags.pop()
        self.fb2_curtag = self.fb2_prevtags[-1]

    def new_p(self):
        self.tobody()
        self.down('p')

    def ensure_in_p(self):
        if self.fb2_curtag == self.fb2_body:
            self.new_p()

    def add_text(self, text):
        if not self.fb2_curtag.getchildren():
            if not self.fb2_curtag.text:
                self.fb2_curtag.text = text
            else:
                self.fb2_curtag.text += text
        else:
            last_child = self.fb2_curtag.getchildren()[-1]
            if last_child.tail:
                last_child.tail += text
            else:
                last_child.tail = unicode(text)

    def include_element(self, element):
        if isinstance(element, unicode):
            self.add_text(element)
            return

        fb2tn = html_to_fb2_tags.get(element.name)
        if fb2tn is None and 'id' in element.attrs:
            fb2tn = 'style'

        if fb2tn != 'p':
            self.ensure_in_p()

        if fb2tn is None:
            for subelement in element:
                self.include_element(subelement)
        elif fb2tn == 'p':
            self.new_p()
            try:
                self.fb2_curtag.attrib['id'] = element.attrs['id']
            except KeyError:
                pass
            for subelement in element:
                self.include_element(subelement)
            self.tobody()
        elif fb2tn == 'image':
            try:
                src = element.attrs['src']
            except KeyError:
                return
            try:
                img_stream = urlopen(src)
            except ValueError:
                return
            except HTTPError:
                return
            except Exception:
                return

            try:
                image = img_stream.read()
            except Exception:
                pass

            # FIXME Decuce extension based on image contents
            img_ext = 'xxx'
            img_name = str(self.img_idx) + '.' + img_ext

            self.down(fb2tn)
            self.fb2_curtag.attrib['l:href'] = '#' + img_name

            # FIXME determine mime
            self.images.append(ImageData(name=img_name, mime=None, image=image, fb2tag=self.fb2_curtag))
        else:
            self.down(fb2tn)
            if fb2tn == 'a':
                try:
                    self.fb2_curtag.attrib['l:href'] = element.attrs['href']
                except KeyError:
                    pass
            for subelement in element:
                self.include_element(subelement)

        if fb2tn != 'p':
            try:
                self.fb2_curtag.attrib['id'] = element.attrs['name']
            except KeyError:
                pass
            try:
                self.fb2_curtag.attrib['id'] = element.attrs['id']
            except KeyError:
                pass
            if fb2tn is not None:
                self.up()

def transform_html(html_top, fb2):
    transformer = HTMLTransformer(fb2)
    for element in html_top.find('body'):
        transformer.include_element(element)
    return transformer.images

def transform_html_stream(html_stream, fb2):
    html = parse(html_stream, 'html5lib')
    return transform_html(html, fb2)
