# vim:fileencoding=utf-8:sts=4:sw=4:et


from __future__ import unicode_literals
import re


NBSP = '\u00A0'


reduced_tags = set(('emphasis', 'strong', 'strikethrough', 'a'))
not_reduced_with_spaces = set(('strikethrough',))


def index(parent, cur):
    i = 0
    for child in parent:
        if child is cur:
            return i
        i += 1
    raise ValueError()


def squash_adjacent(prev, toprocess, parent):
    if (
        not toprocess
        or prev.tag not in reduced_tags
        or (prev.tail and (prev.tail.strip() or prev.tag in not_reduced_with_spaces))
    ):
        return None
    cur = toprocess[0]
    if prev.tag == cur.tag and prev.attrib == cur.attrib:
        toprocess.pop(0)
        if prev.tail:
            if len(prev):
                add_text(prev[-1], prev.tail, 'tail')
            else:
                add_text(prev, prev.tail)
        prev.tail = cur.tail
        cur.tail = None
        prev.append(cur)
        rm_element(prev, cur)
        parent.remove(cur)
    return False


def add_text(element, text, attr='text'):
    if text:
        setattr(element, attr, (getattr(element, attr) or '') + text)


def rm_element(parent, child):
    idx = index(parent, child)
    if child.text:
        text = child.text
    else:
        text = ''
    last_subchild = None
    for subchild in reversed(child):
        if last_subchild is None:
            last_subchild = subchild
        parent.insert(idx, subchild)
    if child.tail:
        if last_subchild is None:
            text += child.tail
        else:
            add_text(last_subchild, child.tail, 'tail')
    if text:
        if idx == 0:
            add_text(parent, child.text)
        else:
            add_text(parent[idx - 1], child.text, 'tail')
    parent.remove(child)


def squash_nested(cur, parents):
    if cur.tag not in reduced_tags:
        return None
    if cur.tag in (t.tag for t in parents):
        rm_element(parents[-1], cur)
    return False


def _strip_leading_spaces(cur):
    if cur.text:
        cur.text = cur.text.lstrip().lstrip(NBSP)
    if not cur.text:
        cur.text = None
        try:
            cur = next(iter(cur))
        except StopIteration:
            return False
        return _strip_leading_spaces(cur)
    return False


def strip_leading_spaces(cur):
    if cur.tag != 'p':
        return None
    return _strip_leading_spaces(cur)


def _strip_trailing_spaces(cur):
    if cur.tail:
        cur.tail = cur.tail.rstrip().rstrip(NBSP)
    if not cur.tail:
        cur.tail = None
        if len(cur):
            return _strip_trailing_spaces(cur[-1])
        elif cur.text:
            cur.text = cur.text.rstrip().rstrip(NBSP)
            if not cur.text:
                cur.text = None
    return False


def strip_trailing_spaces(cur, parent, toprocess):
    if not ((parent.tag == 'p' and not toprocess) or (cur.tag == 'p' and not len(cur))):
        return None
    return _strip_trailing_spaces(cur)


def empty_paragraph_to_empty_line(cur):
    if cur.tag == 'p' and not (cur.text and cur.text.strip()) and not len(cur):
        cur.tag = 'empty-line'
        cur.text = None
        return True
    return None


def del_empty_tags(cur, parents):
    if cur.tag not in reduced_tags or cur.text or len(cur):
        return False
    rm_element(parents[-1], cur)
    del_empty_tags(parents[-1], parents[:-1])
    return True


def apply_text_fun(element, fun):
    if element.text:
        element.text = fun(element.text)
    if element.tail:
        element.tail = fun(element.tail)


def find_prev_symbol_in(element):
    if element.tail:
        return element.tail[-1]
    elif len(element):
        return find_prev_symbol_in(element[-1])
    elif element.text:
        return element.text[-1]
    else:
        return None


def find_prev_symbol_text(cur, parents):
    if cur.tag == 'p':
        return None
    else:
        for parent in reversed(parents):
            if len(parent):
                idx = index(parent, cur)
                if idx:
                    return find_prev_symbol_in(parent[idx - 1])
            if parent.tag == 'p':
                return None
            cur = parent
        return None


def find_prev_symbol_tail(cur, parents):
    return find_prev_symbol_in(cur) or find_prev_symbol_text(cur, parents)


def find_prev_symbol_attr(attr, start, cur, *args, **kwargs):
    if start:
        return getattr(cur, attr)[start - 1]
    return globals()['find_prev_symbol_' + attr](cur, *args, **kwargs)


SPACE_SYMBOLS = ' ' + NBSP

SPACE_REG_PART = '(?:\s|' + NBSP + ')*'


EMDASH = '—'

DASH_REG = re.compile('(' + SPACE_REG_PART + ')-+')


def transform_dash_match(attr, cur, parents, match):
    if match.group(1):
        return NBSP + EMDASH
    else:
        start = match.start()
        prev_symbol = find_prev_symbol_attr(attr, start, cur, parents)
        if prev_symbol is None or prev_symbol in SPACE_SYMBOLS:
            return EMDASH
    return match.group()

def transform_dash(attr, cur, parents):
    text = getattr(cur, attr)
    if text:
        setattr(cur, attr, DASH_REG.sub(lambda match: transform_dash_match(attr, cur, parents, match), text))


COPYRIGHT_REG = re.compile('\\([cCсС]\\)')
NBSP_REG = re.compile(SPACE_REG_PART + NBSP + SPACE_REG_PART)
SP_REG = re.compile('\s+')
NO_REG = re.compile('(?<=\\s)N(\\s*)(?=\\d)')

def transform_characters(cur, parents):
    # Simple transformations: ellipsis, copyright sign, spaces, number sign
    apply_text_fun(cur, lambda text: '…'.join(text.split('...')))
    apply_text_fun(cur, lambda text: COPYRIGHT_REG.sub('©', text))
    apply_text_fun(cur, lambda text: NBSP_REG.sub(NBSP, text))
    apply_text_fun(cur, lambda text: SP_REG.sub(' ', text))
    apply_text_fun(cur, lambda text: NO_REG.sub('№\\1', text))

    # Complex transformations: dash
    transform_dash('text', cur, parents)
    transform_dash('tail', cur, parents)


NEEDS_CURRENT   = 0
NEEDS_PARENT    = 1
NEEDS_PARENTS   = 2
NEEDS_TOPROCESS = 4


def apply_filters(filters, parent, parents):
    assert parents or parent.tag == 'body'
    parents = parents + (parent,)
    toprocess = list(parent)
    while toprocess:
        cur = toprocess.pop(0)
        for attr, filt in filters:
            kwargs = {}
            if attr & NEEDS_PARENT:
                kwargs['parent'] = parent
            if attr & NEEDS_PARENTS:
                kwargs['parents'] = parents
            if attr & NEEDS_TOPROCESS:
                kwargs['toprocess'] = toprocess
            if filt(cur, **kwargs):
                break


def apply_all_filters(parent):
    for filters in all_filters:
        filters += ((NEEDS_PARENTS, lambda *args, **kwargs: apply_filters(filters, *args, **kwargs)),)
        apply_filters(filters, parent, ())


all_filters = (
    (
        (NEEDS_CURRENT,                empty_paragraph_to_empty_line),
        (NEEDS_CURRENT,                strip_leading_spaces         ),
        (NEEDS_PARENT|NEEDS_TOPROCESS, strip_trailing_spaces        ),
        (NEEDS_PARENTS,                del_empty_tags               ),
        (NEEDS_PARENT|NEEDS_TOPROCESS, squash_adjacent              ),
        (NEEDS_PARENTS,                squash_nested                ),
    ),
    (
        (NEEDS_PARENTS,                transform_characters         ),
    ),
)
